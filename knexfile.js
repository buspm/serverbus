// Update with your config settings.

module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host:'localhost',
      user:'root',
      password:'',
      database:'db_bus'
    },
    migrations:{
      directory:__dirname+ '/db/migrations',
    },
    seeds:{
      directory:__dirname+ '/db/seeds',
    }
  },
};
